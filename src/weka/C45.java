/*
 * Created on 2004-11-04
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package weka;

import java.io.File;
import java.io.IOException;

import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ArffLoader;


/**
 * @author mdramins
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class C45
{
    private ArffLoader arffLoader;
    protected J48 j48Tree;
//  ****************************************************    
    public C45()
    {
        j48Tree = new J48();
        setDefaultParameters();
        arffLoader = new ArffLoader();
    }
//  ****************************************************    
    public static void main(String[] args)
    {
        C45 c45=new C45();
        File f = new File("./data/weather.arff");
        System.out.println(f.getAbsolutePath());
        System.out.println(f.exists());
        
        c45.train(f.getAbsolutePath());
        c45.j48Tree.setPrintNodeIndicators(true);
        System.out.println(c45.j48Tree.toString());
    }
//  ****************************************************
  public void train(String inputArff)
  {
    Instances wekaTrainInstances;
    String trainFile =  inputArff;

    try
    {
      arffLoader.setFile(new File(trainFile));
      wekaTrainInstances = arffLoader.getDataSet();
      wekaTrainInstances.setClass(wekaTrainInstances.attribute("play"));
    }
    catch (IOException e)
    {
      System.out.println("Error loading Arff files!");
      e.printStackTrace();
      return;
    }
    try
    {
      j48Tree.buildClassifier(wekaTrainInstances);
    }
    catch (Exception e)
    {
      System.out.println("J48 Unexpected Exception!");
      e.printStackTrace();
      return;
    }

  }
//****************************************************
  public void setDefaultParameters()
  {
//J48 Parameters
    j48Tree.setBinarySplits(false);
    j48Tree.setMinNumObj(1);
    j48Tree.setSaveInstanceData(false);
//The confidence factor used for pruning (smaller values incur more pruning
    j48Tree.setConfidenceFactor( (float) 0.25);
//Whether reduced-error pruning is used instead of C.4.5 pruning
    j48Tree.setReducedErrorPruning(false);
//Whether to consider the subtree raising operation when pruning
    j48Tree.setSubtreeRaising(true);
    /* Determines the amount of data used for reduced-error pruning
      One fold is used for pruning, the rest for growing the tree */
    j48Tree.setNumFolds(3);
    j48Tree.setUnpruned(false);
//Whether counts at leaves are smoothed based on Laplace
    j48Tree.setUseLaplace(false);
  }
//****************************************************  
}


